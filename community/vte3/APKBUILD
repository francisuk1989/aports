# Contributor: Rasmus Thomsen <oss@cogitri.dev>
# Maintainer: Natanael Copa <ncopa@alpinelinux.org>
pkgname=vte3
pkgver=0.76.2
pkgrel=0
pkgdesc="Virtual Terminal Emulator library"
url="https://gitlab.gnome.org/GNOME/vte"
arch="all"
license="LGPL-2.0-or-later"
subpackages="$pkgname-dbg $pkgname-dev $pkgname-gtk4 $pkgname-lang"
makedepends="
	bash
	gnutls-dev
	gobject-introspection-dev
	gperf
	gtk+3.0-dev
	gtk4.0-dev
	gtk-doc
	icu-dev
	libxml2-utils
	linux-headers
	lz4-dev
	meson
	ncurses-dev
	pango-dev
	pcre2-dev
	vala
	"
source="https://gitlab.gnome.org/GNOME/vte/-/archive/$pkgver/vte-$pkgver.tar.gz
	fix-W_EXITCODE.patch
	"

builddir="$srcdir/vte-$pkgver"

build() {
	abuild-meson \
		-Dgtk4=true \
		-Ddocs=false \
		-D_systemd=false \
		. output
	meson compile -C output
}

check() {
	meson test --no-rebuild --print-errorlogs -C output
}

package() {
	DESTDIR="$pkgdir" meson install --no-rebuild -C output
}

gtk4() {
	pkgdesc="$pkgdesc (gtk4 component)"

	amove usr/bin/vte-*-gtk4
	amove usr/lib/libvte-*-gtk4.so.*
}

sha512sums="
290ba842af5e8b0f8822eeb9e61c145b989dc333dd3e1fa2a27bc8a16e8cefa5f238b560083c3c80251b643acd56f8cc09c6bf51056d7540721815a8ddb68678  vte-0.76.2.tar.gz
b6c1856bf075c2e3e91a0d4aff700c59e738bd6abe4122a11d680f104a2dab9d99f7d836a3ef3020b25ceff0a37231a6561eb917f0e4b9f90837eb634d8f7f20  fix-W_EXITCODE.patch
"
